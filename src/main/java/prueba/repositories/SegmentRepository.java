//package falabella.repositories;
//
//import falabella.enumerators.CountryIdEnumerator;
//import falabella.models.Segment;
//import org.springframework.data.mongodb.repository.MongoRepository;
//
//public interface SegmentRepository extends MongoRepository<Segment, String> {
//
//    Segment findByCountryIdAndStatusIdAndSegmentCode(
//            CountryIdEnumerator countryId,
//            Boolean statusId,
//            String segmentCode);
//}
