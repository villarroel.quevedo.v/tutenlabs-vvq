package prueba.jsons.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class TimeRequest {

    public String dato1;
    public String dato2;
    
    public TimeRequest() {
    }

    public TimeRequest(
            
            String dato1,
            String dato2) {
        //super(statusId, creationDate, updateDate);
        this.dato1 = dato1;
        this.dato2 = dato2;
    }

    @Override
    public String toString() {
        return "TimeRequest{" +
                "dato1='" + dato1 + '\'' +
                ", dato2='" + dato2 + '\'' +
                '}';
    }

	public String getDato1() {
		return dato1;
	}

	public void setDato1(String dato1) {
		this.dato1 = dato1;
	}

	public String getDato2() {
		return dato2;
	}

	public void setDato2(String dato2) {
		this.dato2 = dato2;
	}
    
    
}