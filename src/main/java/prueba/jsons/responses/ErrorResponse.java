package prueba.jsons.responses;

import prueba.enumerators.ErrorCodeEnumerator;

public class ErrorResponse {

    private ErrorCodeEnumerator code;
    private String message;

    public ErrorResponse() {
    }

    public ErrorResponse(ErrorCodeEnumerator code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

    public ErrorCodeEnumerator getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
