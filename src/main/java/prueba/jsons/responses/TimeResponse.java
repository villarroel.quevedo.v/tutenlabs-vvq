package prueba.jsons.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimeResponse {

	private String time;
	private String timeZone;
}
