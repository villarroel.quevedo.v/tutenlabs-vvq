package prueba.controllers;




import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import prueba.exceptions.CustomException;
import prueba.jsons.request.TimeRequest;
import prueba.jsons.responses.ErrorResponse;
import prueba.jsons.responses.Response;
import prueba.services.TimeService;


@Validated
@RestController
@RequestMapping("/time")
public class TimeController {

    @Autowired
    TimeService timeService;
//
    
    @PostMapping("/obtener")
    public ResponseEntity<?> postTime(
            @Valid
            @RequestBody
            TimeRequest timeRequest){

            Response resp = new Response();

            try{
                resp = timeService.obtener(
                        timeRequest
                );
            }catch (CustomException e){
                return new ResponseEntity<>(
                        new ErrorResponse(
                                e.getErrorType(),
                                e.getMessage()),
                        e.getErrorType().getCode());

            }

            return new ResponseEntity<>(
                    resp,
                    HttpStatus.CREATED
            );

    }
    
    @GetMapping
    ResponseEntity<?> getTime(){
    	
    	Response resp;
    	
    	try {
    		
    		resp = timeService.getTime();
    		
    	}catch(CustomException e) {
    		
          return new ResponseEntity<>(
		         new ErrorResponse(
		                 e.getErrorType(),
		                 e.getErrorType().getDescription()),
		          		 e.getErrorType().getCode());
		    	}
			return new ResponseEntity<>(resp, HttpStatus.OK);
    	
    	}

    }

