package prueba.services;

import prueba.constants.Constants;
import prueba.exceptions.CustomException;
import prueba.jsons.request.TimeRequest;
//import falabella.enumerators.ChannelIdEnumerator;
//import falabella.enumerators.CommerceIdEnumerator;
//import falabella.enumerators.ConsumerIdEnumerator;
//import falabella.enumerators.CountryIdEnumerator;
//import falabella.enumerators.ErrorCodeEnumerator;
import prueba.jsons.responses.Response;
import prueba.jsons.responses.TimeResponse;
import prueba.models.Time;

//import prueba.repositories.SegmentRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Vanessa Villarroel
 *
 */
@Service
public class TimeServiceImpl implements TimeService{

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());


    @Override
	public Response getTime() throws CustomException {
		// TODO Auto-generated method stub
    	
    	Response resp = new Response();
    	TimeResponse timeResp = null;
    	Time timeClass = new Time();
          
          /**/
          
          LocalTime time1 = LocalTime.of(18,31,45);  
          
          LocalTime time2=time1.plusHours(4);  
          
          timeClass.setTime(time2.toString());
    	  timeClass.setTimeZone(TimeZone.getTimeZone("UTC").getID());
        
    	  
    	  
          try {
        	  timeResp = new ModelMapper().map(
        			timeClass,
  					TimeResponse.class);
  					LOG.info("timeResponse:[{}]", timeResp);
  			
	  			resp = new ModelMapper().map(
	  					timeResp,
	  					Response.class);
	  					LOG.info("timeResponse:[{}]", resp);
  			
          }catch(Exception e) {
  			LOG.error("ERROR :", e);
  			throw new CustomException(e.toString());
  		}
          return resp;
		
	}


	@Override
	public Response obtener(TimeRequest request) {
		// TODO Auto-generated method stub
		Response resp = new Response();
    	TimeResponse timeResp = null;
    	Time timeClass = new Time();
    	String time;
    	String hora;
    	String minuto;
    	String segundo;
      
          /*00:00:00*/

          time = request.dato1;
          String[] parts = time.split(":");
          hora = parts[0]; // 00
          minuto = parts[1]; // 00
          segundo = parts[2]; // 00
          
          
          int h = Integer.parseInt(hora);
          int m = Integer.parseInt(minuto);
          int s = Integer.parseInt(segundo);
          int utc = Integer.parseInt(request.dato2);
          
          LocalTime dato1 = LocalTime.of(h,m,s);  
          
          LocalTime dato2 = dato1.plusHours(utc);  
          
          timeClass.setTime(dato2.toString());
    	  timeClass.setTimeZone(TimeZone.getTimeZone("UTC").getID());
    	  
    	  
    	  
          try {
        	  timeResp = new ModelMapper().map(
        			timeClass,
  					TimeResponse.class);
  					LOG.info("timeResponse:[{}]", timeResp);
  			
	  			resp = new ModelMapper().map(
	  					timeResp,
	  					Response.class);
	  					LOG.info("timeResponse:[{}]", resp);
  			
          }catch(Exception e) {
  			LOG.error("ERROR :", e);
  			throw new CustomException(e.toString());
  		}
	     
		return resp;
	}
	
	

}
