package prueba.services;


import java.util.List;

import prueba.exceptions.CustomException;
import prueba.jsons.request.TimeRequest;
import prueba.jsons.responses.Response;

public interface TimeService {
	
 Response getTime() throws CustomException;
 Response obtener(TimeRequest request);
}
