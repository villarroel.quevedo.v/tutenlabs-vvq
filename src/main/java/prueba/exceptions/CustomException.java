package prueba.exceptions;

import prueba.enumerators.ErrorCodeEnumerator;

public class CustomException extends RuntimeException {

    private ErrorCodeEnumerator errorType;

    public CustomException(String message){
        super(message);
        this.errorType = ErrorCodeEnumerator.DEFAULT_ERROR;
    }

    public CustomException(String message, ErrorCodeEnumerator errorType) {
        super(message);
        this.errorType = errorType;
    }

    public ErrorCodeEnumerator getErrorType() {
        return errorType;
    }
}
