package prueba.utils;

import prueba.constants.Constants;
import prueba.enumerators.ErrorCodeEnumerator;
import prueba.exceptions.CustomException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RestTemplateHelper {

    @Autowired
    private RestTemplate restTemplate;

    private ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public <T> Map<String, ?> get(
            Class<T> clazz,
            String url,
            HttpHeaders headers,
            Object... uriVariables) throws CustomException {

        try {
            HttpEntity<?> httpEntity = new HttpEntity<>(headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    httpEntity,
                    String.class,
                    uriVariables);

            JavaType javaType = objectMapper.getTypeFactory().constructType(clazz);

            return readValue(
                    response,
                    javaType);
        }
        catch (HttpClientErrorException e) {
            manageException(e);
        }
        catch (Exception e){
            throw new CustomException(e.getMessage());
        }

        return null;
    }

    public <T> Map<String, ?> getList(
            Class<T> clazz,
            String url,
            HttpHeaders headers,
            Object... uriVariables) throws CustomException {

        try {
            HttpEntity<?> httpEntity = new HttpEntity<>(headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    httpEntity,
                    String.class,
                    uriVariables);

            CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(
                    List.class,
                    clazz);

            return readValue(
                    response,
                    collectionType);
        }
        catch (HttpClientErrorException e) {
            manageException(e);
        }
        catch (Exception e){
            throw new CustomException(e.getMessage());
        }

        return null;
    }

    public <T, R> Map<String, ?> put(
            Class<T> clazz,
            String url,
            R body,
            HttpHeaders headers,
            Object... uriVariables) throws CustomException {

        try {
            HttpEntity<R> request = new HttpEntity<>(body, headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    url,
                    HttpMethod.PUT,
                    request,
                    String.class,
                    uriVariables);

            JavaType javaType = objectMapper.getTypeFactory().constructType(clazz);

            return readValue(
                    response,
                    javaType);
        }
        catch (HttpClientErrorException e) {
            manageException(e);
        }
        catch (Exception e){
            throw new CustomException(e.getMessage());
        }

        return null;
    }

    public <T, R> Map<String, ?> post(
            Class<T> clazz,
            String url,
            R body,
            HttpHeaders headers,
            Object... uriVariables) throws CustomException {

        try{
            HttpEntity<R> request = new HttpEntity<>(body, headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    request,
                    String.class,
                    uriVariables);

            JavaType javaType = objectMapper.getTypeFactory().constructType(clazz);

            return readValue(
                    response,
                    javaType);
        }
        catch (HttpClientErrorException e) {
            manageException(e);
        }
        catch (Exception e){
            throw new CustomException(e.getMessage());
        }

        return null;
    }

    public Map<String, ?> delete(
            String url,
            HttpHeaders headers,
            Object... uriVariables) throws CustomException {

        try {
            HttpEntity<?> request = new HttpEntity<>(headers);
            Map<String, Object> responseMap = new HashMap<>();

            ResponseEntity<String> response = restTemplate.exchange(
                    url,
                    HttpMethod.DELETE,
                    request,
                    String.class,
                    uriVariables);

            responseMap.put(Constants.MAP_HTTP_STATUS_CODE, response.getStatusCode());

            return responseMap;
        }
        catch (HttpClientErrorException e) {
            manageException(e);
        }
        catch (Exception e){
            throw new CustomException(e.getMessage());
        }

        return null;
    }

    private Map<String, ?> readValue(
            ResponseEntity<String> response,
            JavaType javaType) throws CustomException {

        try {
            Map<String, Object> responseMap = new HashMap<>();

            if (response.getStatusCode() == HttpStatus.OK
                    || response.getStatusCode() == HttpStatus.CREATED) {

                Object result = objectMapper.readValue(
                        response.getBody(),
                        javaType);

                responseMap.put(Constants.MAP_RESPONSE, result);
            }

            responseMap.put(Constants.MAP_HTTP_STATUS_CODE, response.getStatusCode());

            return responseMap;
        }
        catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    private <T> T manageException(HttpClientErrorException exception) throws CustomException {

        switch (exception.getStatusCode()) {
            
            default :
                throw new CustomException(exception.getMessage(), ErrorCodeEnumerator.DEFAULT_ERROR);
        }
    }

}