package prueba.constants;

public abstract class Constants {

    public static final String MAP_RESPONSE= "response";
    public static final String MAP_HTTP_STATUS_CODE= "httpStatusCode";

    //Mensajes de error para capa de presentación
    public static final String DEFAULT_ERROR = "Error interno";

}
