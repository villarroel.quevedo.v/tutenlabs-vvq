package prueba.enumerators;

import prueba.constants.Constants;
import org.springframework.http.HttpStatus;

public enum ErrorCodeEnumerator {

    DEFAULT_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, Constants.DEFAULT_ERROR);

    private final HttpStatus code;
    private final String description;

    ErrorCodeEnumerator(HttpStatus code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String toString() {
        return "ErrorCodeEnumerator{" +
                "code=" + code +
                ", description='" + description + '\'' +
                '}';
    }

    public HttpStatus getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}