package prueba.models;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.springframework.data.annotation.TypeAlias;

@TypeAlias("time")
public class Time{

	//OffsetDateTime now = OffsetDateTime.now(ZoneOffset.UTC);
	private String time;
	private String timeZone;

    public Time() {
    }

    public Time(
            
    		String time, String timeZone) {
        //super(statusId, creationDate, updateDate);
        this.time = time;
        this.timeZone = timeZone;
  
    }

    @Override
    public String toString() {
        return "Time{" +
                "time='" + time + '\'' +
                ", timezone='" + timeZone + '\'' +
                '}';
    }

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

   
}
