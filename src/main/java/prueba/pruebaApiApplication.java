package prueba;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.web.client.RestTemplate;

import prueba.jsons.request.TimeRequest;
import prueba.models.Time;


//@RefreshScope
@SpringBootApplication
public class pruebaApiApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(pruebaApiApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub

		final String uri = "http://localhost:8081/time/obtener";
  	    RestTemplate restTemplate = new RestTemplate();
  	    
  	    
  	    TimeRequest request = new TimeRequest();
  	    request.setDato1("10:01:01");
  	    request.setDato2("4");
  	   
  	    Time time = restTemplate.postForObject(uri, request, Time.class);

	    
  	    System.out.println("TIME------------------->" + time);
	}
}
