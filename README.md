1-para obtener el servicio se debe bajar el proyecto con git clone https://gitlab.com/villarroel.quevedo.v/tutenlabs-vvq.git
2-posicionarse en la rama develop y ejecutar** git pull - git fetch**
3-para ejecutar el servicio : **mvn clean install - mvn spring-boot:run**
4-para probar el servicio se sugiere el uso de **POSTMAN** o similar
5.la url del servicio es **localhost:8081/time/obtener** (el puerto se puede configurar en **application.properties** e caso de querer ocupar otro).
6. como es un servicio post el body sería :
{
	"dato1":"12:02:11",
	"dato2":"4"
}

7. el response debería devolver:

{
    "response": {
        "time": "16:02:11",
        "timeZone": "UTC"
    }
}

*Observación: Por falta de tiempo no pude implementar correctamente el cliente en pruebaApiApplication.java para probar por consola.
