FROM openjdk:8
ENTRYPOINT ["mvnw","clean"]
ENTRYPOINT ["mvnw","install","package"]
ADD target/prueba-api.jar prueba-api.jar
EXPOSE 80
ENTRYPOINT ["java","-jar","prueba-api.jar"]